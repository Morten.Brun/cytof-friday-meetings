# CyTOF Friday meetings

This repository is intented to contain examples of work flows for CyTOF data.

## R scripts

## Jupyter notebooks.

## Installation of Divisive Gater

### Prerequisites:

* Python 3
* Jupyter Notebook

### Install a git client

Maybe you can [use these instructions](https://docs.gitlab.com/ce/topics/git/how_to_install_git/index.html).

### Recommendation: Install python 3 with [Anaconda](https://www.anaconda.com/) for your OS. 

Once [Anaconda](https://www.anaconda.com/)
is installed, if you are using Windows, open the
Anaconda Prompt and start jupyter by typing `jupyter notebook` at the
prompt. (Typing `jupyter notebook` at the commando line also works.)
If you are using Linux or macOS, open a terminal and start
jupyter by typing `jupyter notebook` at the prompt. 

#### Optional: Set up virtual environments in Anaconda

In order to be able to run virtual environments in jupyter notebook a
tweek needs to be done: You need to install
[`nb_conda_kernels`](https://github.com/Anaconda-Platform/nb_conda_kernels). For
linux users, [this seems to be a good
guide](https://www.charles-deledalle.fr/pages/files/configure_conda.pdf)  

I do not know how to install `nb_conda_kernels` in the GUI. Please
inform me if you figure out a way to do that.

### Clone the [divisivegater](https://git.app.uib.no/Morten.Brun/divisivegater) repository.

If you have trouble with ssh connection, use the https alternative.

Follow the installation procedures of the
[divisivegater](https://git.app.uib.no/Morten.Brun/divisivegater)  by 
implementing all the commands starting the git clone command. 

Optional: After installing `divisivegater` and opening jupyter you need to tell
jupyter that you want to use `divisivegater`. You do this by choosing 
`divisivegater` as 'kernel'. 


### Test the installation
 
Use jupyter notebook to navigate to the python folder of this
repository and see if the notebooks in there work. If not, please send
me an email explaining what goes wrong. 

### Programming projects:

It will be nice to add functionality for interaction with
cytobank. This is easy. In particular for those who have an account at
cytobank. Have a look at [the cytobank developer
information](https://developer.cytobank.org/?version=latest). 
